package com.amen.sda.files.zadanie1;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class MainZadanie1 {

    public static void main(String[] args) {
        // 1. otwieramy plik
        // 2. dopisujemy w petli
        // 3. jesli wpiszemy quit, to zamknij > wyjdz z petli


        Scanner scanner = new Scanner(System.in);
        // poniżej linia otwarcie pliku (robimy to 1 raz)
        try (PrintWriter writer = new PrintWriter(new FileWriter("plikX.txt", true))) { // spowoduje otwarcie pliku
            // -> w przypadku braku plik tworzy go

            // w petli czytam linie
            while (scanner.hasNextLine()) { // dopoki mamy wejscie
                String linia = scanner.nextLine(); // wczytuje linie z wejscia

                writer.println(linia); // zapisuje wejscie do pliku
                if (linia.equals("quit")) {
                    break; // jesli wczytana linia to quit to przerywam petle
                }

//                writer.flush();
                // jesli to wywolamy po kazdym obiegu,
                // to co obieg dopisze nam do pliku ostatnia linie
            }
        } catch (FileNotFoundException e) { // automatycznie po opuszczeniu try-catch
            // aplikacja zwalnia zasób - tj. zamyka plik zapisując zmiany na dysk
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
