package com.amen.sda.files.zadanie1.zapis;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class PrzykladPliki {
    public static void main(String[] args) {
        // BufferedReader < czytania
        // PrintWriter > zapis

        try(PrintWriter writer = new PrintWriter(new FileWriter("plik.txt", true))){

            writer.println("tekst\nawdawdawdawd");
//            writer.flush(); // wyczyszczenie bufora i wysłanie zmian na dysk
//            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
